import Fingerprint2 from 'fingerprintjs2'

export default ({ app }, inject) => {
  let fingerprint
  const getFingerprint = () =>
    new Promise((resolve) => {
      Fingerprint2.get(
        {
          excludeJsFonts: true,
          excludeFlashFonts: true
        },
        function(components) {
          const values = components.map(function(component) {
            return component.value
          })
          fingerprint = Fingerprint2.x64hash128(values.join(''), 31)
          resolve(fingerprint)
        }
      )
    })
  inject('fingerprint', getFingerprint())
}
