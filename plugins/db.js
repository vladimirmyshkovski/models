const IPFS = require('ipfs')
// const IpfsClient = require('ipfs-http-client')

const OrbitDB = require('orbit-db')

const initIPFSInstance = async (repo) => {
  const ipfs = await IPFS.create({
    repo,
    start: true,
    preload: {
      enabled: false
    },
    EXPERIMENTAL: {
      pubsub: true
    },
    config: {
      Bootstrap: [],
      Addresses: {
        Swarm: [
          // Use IPFS dev signal server
          // '/dns4/star-signal.cloud.ipfs.team/wss/p2p-webrtc-star',
          // '/dns4/ws-star.discovery.libp2p.io/tcp/443/wss/p2p-websocket-star'
          // Use local signal server
          // '/ip4/0.0.0.0/tcp/9090/wss/p2p-webrtc-star',
        ]
      }
    }
  })
  return ipfs
}

const models = initIPFSInstance('ipfs/data/models').then(async (ipfs) => {
  const orbitdb = await OrbitDB.createInstance(ipfs)

  const db = await orbitdb.docstore(
    '/orbitdb/zdpuAvNJ8c1GTdXMpENfvYoL9P3D7LZpFHoZmGpFLYiXuQwC5/models',
    // 'models', // '/orbitdb/zdpuAvNJ8c1GTdXMpENfvYoL9P3D7LZpFHoZmGpFLYiXuQwC5/models',
    {
      accessController: { write: ['*'] }
    }
  )

  // console.log('models db', db)

  console.log('models address', db.address.toString())

  // const db = await orbitdb.docstore(
  // '/orbitdb/zdpuAvNJ8c1GTdXMpENfvYoL9P3D7LZpFHoZmGpFLYiXuQwC5/models'
  // '/orbitdb/zdpuAscVEkLH6a4VHdskvtJJtytiWLgiHqrLnBqc6qZDaxPaS/models'
  // '/orbitdb/zdpuApThSKPcEtB7yWhVcpMyKAS4j1ajJ8ePcuBF29aXvqrjQ/models'
  // '/orbitdb/zdpuAzmNJumkQ43FkUtH1zRWXA6FdGxTjJBdsLog1eF87zfS9/models'
  // '/orbitdb/zdpuB2aiHGkQNpRd1hoCZ9m6FmS6wxuSX4QpgdpCbaLQxsETB/models'
  // )
  // console.log('db models address', db.address.toString())
  await db.load()

  db.events.on('replicated', (address) => {
    console.log('models replicated', address)
    // console.log('models', db.iterator({ limit: -1 }).collect())
  })

  // const result = db.iterator({ limit: -1 }).collect()
  // console.log('result of models', JSON.stringify(result, null, 2))

  console.log(await orbitdb._ipfs.swarm.peers())

  return db
})

const users = initIPFSInstance('ipfs/data/users').then(async (ipfs) => {
  const orbitdb = await OrbitDB.createInstance(ipfs)

  const db = await orbitdb.docstore(
    '/orbitdb/zdpuAm2QNGcdGWKBk258SbCsZ2y8VtN29cDhJTAC4hgxD7C8S/users',
    // 'users', // '/orbitdb/zdpuAm2QNGcdGWKBk258SbCsZ2y8VtN29cDhJTAC4hgxD7C8S/users',
    {
      accessController: { write: ['*'] }
    }
  )

  // console.log('users db', db)

  // console.log('users address', db.address.toString())
  // const db = await orbitdb.open(
  // '/orbitdb/zdpuAm2QNGcdGWKBk258SbCsZ2y8VtN29cDhJTAC4hgxD7C8S/users'
  // '/orbitdb/zdpuAnjx8vqmvNumv66gSyxQZq1tbMpcT2VPsdXXTeM7qhCod/users'
  // '/orbitdb/zdpuB3XCc1iPLzJpkpi4FjeTqmbtMh85xqwNovRtLJpTKjidQ/users'
  // '/orbitdb/zdpuAskVd7M6f3Rbh7xuvJJyGE86S4MgbaUUA413PNJ8cXMK7/users'
  // '/orbitdb/zdpuB3JFCMWjhuDvcBD51Q7bYbdkKZ6fscKdx3fzmKB4wKG3j/users'
  // )
  console.log('db users address', db.address.toString())
  await db.load()

  db.events.on('replicated', (address) => {
    console.log('users replicated', address)
    // console.log('users', db.iterator({ limit: -1 }).collect())
  })

  // const result = db.iterator({ limit: -1 }).collect()
  // console.log('result of users', JSON.stringify(result, null, 2))

  console.log(await orbitdb._ipfs.swarm.peers())

  return db
})

/*
const ipfs = IpfsClient('ipfs.infura.io', '5001', { protocol: 'https' })

const orbitdb = OrbitDB.createInstance(ipfs)

const users = orbitdb.docstore('users')
const models = orbitdb.docstore('models')
*/

export default ({ app }, inject) => {
  inject('models', models)
  inject('users', users)
}
