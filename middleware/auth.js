export default function({ store, redirect, route }) {
  if (!store.state.auth.auth) {
    return redirect(`/auth/signin?next=${route.path}`)
  }
}
